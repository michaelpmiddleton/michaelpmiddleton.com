import { defineConfig, loadEnv } from "vite";
import react from "@vitejs/plugin-react";
import viteTsconfigPaths from "vite-tsconfig-paths";
import svgr from "vite-plugin-svgr";

// https://vitejs.dev/config/
export default ({ mode }) => {
    process.env = { ...process.env, ...loadEnv(mode, process.cwd()) };

    return defineConfig({
        server: {
            port: parseInt(process.env.VITE_API_PORT!),
        },
        plugins: [
            react(),
            viteTsconfigPaths(),
            svgr({
                include: "**/*.svg?react",
            }),
        ],
    });
};
