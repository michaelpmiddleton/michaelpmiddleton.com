module.exports = {
    content: ["./src/**/*.{ts,tsx}"],
    theme: {
        extend: {
            backgroundImage: {
                contact: "url('assets/contact.png')",
            },
        },
    },
    plugins: [],
};
