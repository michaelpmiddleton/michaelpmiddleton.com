import { Component, RefObject, SyntheticEvent } from "react";
import { Button } from "../common";
import emailjs from "@emailjs/browser";
import ReCAPTCHA from "react-google-recaptcha";

import { createRef } from "react";

const labelClasses = "text-slate-300 italic pl-4";

type InputFields = {
    id: string;
    type: string;
    placeholder: string;
    label: string;
};

interface ContactDefaultState {
    senderName: string;
    senderEmail: string;
    senderMessage: string;
    senderIntention: string;
}

const SuccessToast = () => console.log("Message sent!");

const FormatErrorToast = (errorMessages: Array<string>) =>
    console.warn(JSON.stringify(errorMessages));

export default class Contact extends Component<{}, ContactDefaultState> {
    private formRef: RefObject<HTMLFormElement>;
    private captchaRef: RefObject<ReCAPTCHA>;

    constructor(props: any) {
        super(props);
        this.formRef = createRef();
        this.captchaRef = createRef();
        this.state = {
            senderName: "",
            senderEmail: "",
            senderMessage: "",
            senderIntention: "",
        };
    }

    UpdateFormStateValues(key: string, value: string) {
        this.setState((prevState) => ({ ...prevState, [key]: value }));
    }

    Input = (props: InputFields) => {
        return (
            <div className="col-span-3 md:col-span-1" key={props.id}>
                <input
                    id={props.id}
                    name={props.id}
                    type={props.type}
                    placeholder={props.placeholder}
                    className="appearance-none bg-black bg-opacity-0 focus:bg-opacity-40  border-2 text-white placeholder-white focus:outline-none focus:border-purple-400 border-slate-400 rounded-3xl w-full py-2 px-4"
                    onChange={(event) => {
                        event.preventDefault();
                        this.UpdateFormStateValues(props.id, event.target.value);
                    }}
                />
                <label className={labelClasses} htmlFor={props.id}>
                    {props.label}
                </label>
            </div>
        );
    };

    render() {
        const SendEmail = (event: SyntheticEvent) => {
            event.preventDefault();

            this.captchaRef.current?.execute();
            /*  -----------------------------------------------
                                User Input Validation
            ----------------------------------------------- */
            let errorMessages = new Array<string>();
            if (this.state.senderName === "") errorMessages.push("You must enter your name.");
            if (
                // REGEX sourced from: Rnevius' unicode-supporting response on https://stackoverflow.com/questions/46155/whats-the-best-way-to-validate-an-email-address-in-javascript
                this.state.senderEmail === "" ||
                this.state.senderEmail.match(
                    /^(([^<>()[].,;:s@"]+(.[^<>()[].,;:s@"]+)*)|(".+"))@(([^<>()[].,;:s@"]+.)+[^<>()[].,;:s@"]{2,})$/i
                )
            )
                errorMessages.push("You must enter a valid email address.");
            if (this.state.senderIntention === "")
                errorMessages.push("Please select a reason for contacting me.");
            if (this.state.senderMessage === "") errorMessages.push("Please write a message!");
            //-----------------------------------------------

            if (errorMessages.length === 0) {
                if (import.meta.env.VITE_EMAILJS_SERVICE_ID !== undefined && this.formRef.current) {
                    emailjs
                        .sendForm(
                            import.meta.env.VITE_EMAILJS_SERVICE_ID,
                            "contactFormSelfNotify",
                            this.formRef.current,
                            import.meta.env.VITE_EMAILJS_API_PUBLIC_KEY
                        )
                        .then(
                            // Success
                            () => {
                                SuccessToast();
                                this.formRef.current?.reset();
                            },
                            // Failure
                            (err) => {
                                console.error(err);
                                // ClientErrorToast()
                            }
                        );
                } else {
                    console.error();
                    // ClientErrorToast()
                }
            } else {
                FormatErrorToast(errorMessages);
            }
        };

        return (
            <form
                ref={this.formRef}
                className="flex flex-col items-center md:items-start w-full"
                onSubmit={SendEmail}
            >
                {/* <Toaster /> */}
                <h3 className="flex text-5xl  text-center lg:text-left py-16">
                    Send me a message!
                </h3>
                <div className="px-8 py-16">
                    <p className="text-3xl">
                        I'm so glad you want to reach out! With the current version of this website
                        being 2 years old and its backend API in the middle of migration, some
                        features are currently unavailable. At present, the form-based contact me
                        section is one of those features. Please feel to reach out via my social
                        media accounts or my email address. Thank you for understanding!
                    </p>
                </div>
            </form>
        );
    }
}
