import axios from "axios";
import { Component } from "react";
import dayjs from "dayjs";
import { Response, ImageResonse, FormatImageResponse, api } from "../common/api";
import { RequestHeader } from "../common/api/requests";

type Job = {
    title: string;
    company: string;
    startDate: Date; // Might need Moment JS for this...
    endDate?: Date;
    location: string;
    image?: ImageResonse | null;
};

const JobCard = (job: Job) => [
    job.image ? (
        <img
            className="col-span-2 object-scale-down min-h-fit max-h-16 xl:max-h-36 max-w-xs justify-self-center"
            // className="basis-1/2 object-scale-down h-24 xl:h-36 max-w-xs mx-auto"
            src={job.image.url}
            alt={`Company logo for ${job.company}.`}
        />
    ) : (
        <p className="col-span-2 font-medium text-xl min-h-24 place-self-center">{job.company}</p>
    ),
    <div
        key={`${job.title}-${job.company}`}
        className="flex flex-col col-span-1 font-light leading-tight text-slate-300 justify-center"
    >
        <p className="font-medium text-lg text-white">{job.title}</p>
        <p className="uppercase tracking-widest">
            {job.endDate
                ? `${dayjs(job.startDate).format("M/YYYY")} - ${dayjs(job.endDate).format(
                      "M/YYYY"
                  )}`
                : "present"}
        </p>
        <p>{job.location}</p>
    </div>,
];

const SkeletonJobCard = (index: number) => [
    <div
        key={`placeholder-img-${index}`}
        className="h-24 justify-self-center place-self-center w-3/5 bg-slate-600 animate-pulse rounded-3xl"
    ></div>,
    <div key={`placeholder-details-${index}`} className="col-span-2 flex flex-col gap-2">
        <div className="w-full rounded-xl bg-slate-600 animate-pulse h-8" />
        <div className="w-1/3 rounded-xl bg-slate-600 animate-pulse h-6" />
        <div className="w-1/2 rounded-xl bg-slate-600 animate-pulse h-6" />
    </div>,
];

interface AboutDefaultState {
    jobs: Array<Job>;
}
export default class About extends Component<{}, AboutDefaultState> {
    constructor(props: any) {
        super(props);
        this.state = {
            jobs: [],
        };
    }

    getJobs() {
        let { jobs } = this.state;
        const loading = jobs.length === 0;

        if (loading) jobs = [...new Array(4)];

        return jobs.map((job: Job, index) => (loading ? SkeletonJobCard(index) : JobCard(job)));
    }

    async componentDidMount() {
        await axios
            .get<{
                data: Response<Job>;
            }>(`${api}/jobs?populate=*&sort[0]=startDate:desc&pagination[limit]=5`, RequestHeader)
            .then((response) => {
                const jobsCleaned: Array<Job> = [];

                if (Array.isArray(response.data.data)) {
                    response.data.data.map((rawJob) => {
                        let cleaned = rawJob.attributes;

                        // Clean image
                        cleaned.image = cleaned.image.data
                            ? FormatImageResponse(cleaned.image)
                            : null;

                        // Remove extra Strapi field
                        delete cleaned["localizations"];

                        jobsCleaned.push(cleaned);
                        return true;
                    });
                }

                this.setState((prevState) => ({
                    ...prevState,
                    jobs: jobsCleaned,
                }));
            });
    }

    render() {
        let jobs = this.getJobs();
        return (
            <div className="flex flex-row self-center ">
                <div className="flex flex-col gap-16 justify-center lg:basis-1/2 text-center lg:text-left">
                    {/* <div className="flex flex-col gap-16 justify-center lg:basis-1/2 m-8 lg:m-16 xl:m-32 text-center lg:text-left"> */}
                    <h3 className="text-5xl">About Me</h3>
                    <div className="flex flex-col gap-6 leading-7 text-lg">
                        <p>
                            {
                                "Hi there! I'm a developer based in Boston, Massachusetts that enjoys looking at makes things tick."
                            }
                        </p>
                        <p>
                            {
                                'I work primarily in Python (Flask, SQLAlchemy, Pandas), Typescript (Node, React), and C# (Unity, .NET) across my work and personal projects. I\'m a firm believer that no language is a "best language for X" and aspire to keep my toolbelt up-to-date and ever-expanding to be able to solve whatever problem comes my way.'
                            }
                        </p>
                        <p>
                            {
                                "In my free time, one can find me streaming software & game development on Twitch, competing in game jams like Ludum Dare, playing games with friends, or going for walks with my four-legged best friend, Mori."
                            }
                        </p>
                    </div>
                </div>
                {/* TODO: Add prettier scrolling to page section */}
                <div className="basis-0 hidden lg:grid lg:grid-cols-3 lg:basis-1/2 overflow-y-auto h-1/3 gap-4 gap-y-8">
                    {jobs}
                </div>
            </div>
        );
    }
}
