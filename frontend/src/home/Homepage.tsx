import { Component } from 'react'
import { About, RecentWork, Contact } from '.'
import { Footer, PageSection } from '../common'
import hero_transparent from '../assets/hero_transparent.png'
import TwitchWordmark from '../assets/TwitchWordmark.svg'

const Sections = {
    Landing: 'home',
    About: 'about',
    RecentWork: 'recent-work',
    Contact: 'contact',
}

export default class Homepage extends Component {
    render() {
        return (
            <>
                <PageSection
                    id={Sections.Landing}
                    sectionName="Landing"
                    className="pb-0 lg:pb-0 xl:pb-0 justify-center lg:justify-start"
                    topSection
                >
                    <div className="flex flex-col justify-center gap-64 lg:place-items-start ">
                        <div className="text-center lg:text-left">
                            <h1 className="text-4xl py-4 xl:text-6xl font-medium">
                                Michael Middleton
                            </h1>
                            <h2 className="text-xl xl:text-3xl">
                                Programmer, Streamer, & Tinkerer
                            </h2>
                        </div>
                        <div className="hidden lg:block">
                            <p className="uppercase font-medium tracking-widest">
                                Official Affiliate
                            </p>
                            <img
                                className="w-64"
                                src={TwitchWordmark}
                                alt="Wordmark logo for twitch.tv."
                            />
                        </div>
                    </div>
                    <img
                        // className="basis-0 hidden lg:flex md:basis-1/2 w-1/2 place-self-end"
                        className="hidden lg:block absolute bottom-0 right-0 h-5/6 xl:h-full lg:pt-8"
                        src={hero_transparent}
                        alt="Michael sitting on a chair with a transparent background."
                    />
                </PageSection>
                <PageSection id={Sections.About} sectionName="About">
                    <About />
                </PageSection>
                <PageSection id={Sections.RecentWork} sectionName="Recent Work">
                    <RecentWork />
                </PageSection>
                <PageSection id={Sections.Contact} sectionName="Contact Me" className="mb-auto">
                    <Contact />
                </PageSection>
                <Footer />
            </>
        )
    }
}
