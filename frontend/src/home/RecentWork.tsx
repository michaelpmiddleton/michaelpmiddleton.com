import axios from 'axios'
import { Component } from 'react'
import { Project, ProjectTile, isMobileDevice, SkeletonProjectTile } from '../common'
import { RequestHeader } from '../common/api/requests'
import { FormatImageResponse, FormatLabelResponse, Response } from '../common/api/responses'
import ProjectDetailView from '../common/ProjectDetailView'
import { api } from '../common/api'

interface RecentWorkDefaultState {
    projects: Array<Project>
    focusedProject?: Project
    showFocusedProject: boolean
}
export default class RecentWork extends Component<{}, RecentWorkDefaultState> {
    constructor(props: any) {
        super(props)
        this.state = {
            projects: [],
            focusedProject: undefined,
            showFocusedProject: false,
        }
    }

    getHighlightedProjects() {
        let { projects } = this.state
        const loading = projects.length === 0

        if (!loading) {
            // TODO: Convert this to us isMobileDevice to limit the returned data and allow this code to just be
            //              based on this.state.projects, regardless of platform.
            projects = isMobileDevice() ? projects.slice(0, 2) : projects
        } else {
            projects = [...new Array<Project>(isMobileDevice() ? 2 : 5)]
        }

        // Create "see my full portfolio" button after the dynamic resxponse
        if (!projects[projects.length - 1] || projects[projects.length - 1].id > 0)
            projects.push({
                id: -1,
                title: 'See my full portfolio →',
                description: '',
                link: './portfolio',
                locale: 'en',
            })

        return projects.map((project, index) =>
            index === projects.length - 1 ? (
                <ProjectTile key="placeholder" project={project} placeholder show />
            ) : loading ? (
                [SkeletonProjectTile]
            ) : (
                <ProjectTile
                    key={project.title}
                    project={project}
                    focusFunction={() =>
                        this.setState((prevState) => ({
                            ...prevState,
                            focusedProject: project,
                            showFocusedProject: true,
                        }))
                    }
                    show
                />
            )
        )
    }

    async componentDidMount() {
        await axios
            .get<{
                data: Response<Project>
            }>(`${api}/projects?filters[labels][name][$eq]=spotlight&populate=*`, RequestHeader)
            .then((response) => {
                const projectsCleaned: Array<Project> = []

                if (Array.isArray(response.data.data)) {
                    response.data.data.map((rawProj) => {
                        let cleaned = rawProj.attributes

                        // Clean image
                        cleaned.image = cleaned.image.data
                            ? FormatImageResponse(cleaned.image)
                            : null

                        // Clean labels
                        cleaned.labels = cleaned.labels ? FormatLabelResponse(cleaned.labels) : null

                        cleaned.id = rawProj.id

                        // Remove extra Strapi field
                        delete cleaned['localizations']

                        projectsCleaned.push(cleaned)
                        return true
                    })
                }

                this.setState((prevState) => ({
                    ...prevState,
                    projects: projectsCleaned,
                }))
            })
    }

    render() {
        let highlightedProjects = this.getHighlightedProjects()
        return (
            <div className="flex flex-col gap-16 h-fit min-h-min text-center w-full">
                <ProjectDetailView
                    isShowing={this.state.showFocusedProject}
                    projectData={this.state.focusedProject}
                    unfocus={() =>
                        this.setState((prevState) => ({
                            ...prevState,
                            showFocusedProject: false,
                        }))
                    }
                />
                <h3 className="text-5xl lg:text-left w-fit self-center lg:self-start">
                    Recent Work
                </h3>
                <div className="flex flex-col md:grid md:grid-cols-2 xl:grid-cols-3 gap-6">
                    {highlightedProjects}
                </div>
            </div>
        )
    }
}
