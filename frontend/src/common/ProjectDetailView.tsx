import { Transition } from '@headlessui/react'
import { XMarkIcon, ArrowDownTrayIcon } from '@heroicons/react/24/solid'
import { Component } from 'react'
import { Button, parseSourceLocation } from '.'
import { allowDocumentScroll, LabelColorMap } from './Misc'
import { Project } from './Project'

interface ProjectDetailViewProps {
    isShowing: boolean
    projectData?: Project
    unfocus?: () => void
}
export default class ProjectDetailView extends Component<ProjectDetailViewProps, {}> {
    closeView = () => {
        if (this.props.isShowing) {
            allowDocumentScroll(true)
            if (this.props.unfocus && this.props.unfocus !== undefined) this.props.unfocus()
        }
    }

    render() {
        if (this.props.isShowing) allowDocumentScroll(false)
        return (
            <Transition show={this.props.isShowing} className="z-30">
                <Transition.Child
                    enter="transition duration-150 ease-out"
                    enterFrom="transform scale-50 opacity-0"
                    enterTo="transform scale-100 opacity-100"
                    leave="transition duration-100 ease-out"
                    leaveFrom="transform scale-100 opacity-100"
                    leaveTo="transform scale-50 opacity-0"
                    className={
                        'fixed w-full h-full left-0 top-0 z-30 text-center lg:text-left lg:w-2/5 lg:h-fit  lg:left-1/2 lg:top-1/2 lg:-translate-x-1/2 lg:-translate-y-1/2 lg:rounded-lg lg:shadow-xl bg-gray-700 '
                    }
                >
                    <XMarkIcon
                        className="absolute top-8 right-8 lg:top-4 lg:right-4 w-12 text-red-500 animate-pulse"
                        onClick={this.closeView}
                    />
                    <img
                        className="mx-auto mt-32 w-11/12 lg:m-0 rounded-xl lg:w-full lg:rounded-t-lg lg:rounded-b-none"
                        src={
                            this.props.projectData?.image
                                ? this.props.projectData.image.url
                                : 'https://via.placeholder.com/1920x1080/09f/fff.png'
                        }
                        alt="TODO: Make img emmet ADA-compliant (ie. add alt text)"
                    />
                    <div className="flex flex-col gap-10 lg:gap-6 p-4 lg:p-8">
                        <h3 className="text-4xl pt-4 lg:pt-0">{this.props.projectData?.title}</h3>
                        <p>{this.props.projectData?.description}</p>
                        {this.props.projectData?.labels ? (
                            <div className="flex flew-row gap-4 items-center">
                                <p className="font-bold">Categories:</p>
                                <div className="flex flew-row gap-2 flex-wrap basis-11/12">
                                    {this.props.projectData?.labels?.map((label: string) => (
                                        <p
                                            key={label}
                                            className={
                                                (LabelColorMap[label]
                                                    ? LabelColorMap[label]
                                                    : 'bg-slate-200') +
                                                ' py-2 px-4 rounded-full w-min text-black flex whitespace-nowrap capitalize font-medium text-xs'
                                            }
                                        >
                                            {label}
                                        </p>
                                    ))}
                                </div>
                            </div>
                        ) : null}
                        <div className="flex flex-col gap-8 lg:gap-6 xl:flex-row lg:w-full">
                            {this.props.projectData?.sourceCode ? (
                                <Button
                                    label="View Project Source"
                                    link={this.props.projectData.sourceCode}
                                    {...parseSourceLocation(this.props.projectData)}
                                    className={'w-full xl:w-fit '}
                                />
                            ) : null}
                            {this.props.projectData?.download ? (
                                <Button
                                    label="Download Project"
                                    link={this.props.projectData.download}
                                    image={<ArrowDownTrayIcon className="w-8 text-black" />}
                                    className={'w-full xl:w-fit'}
                                />
                            ) : null}
                        </div>
                    </div>
                </Transition.Child>
                <Transition.Child
                    enter="transition duration-50 ease-out"
                    enterFrom="transform opacity-0"
                    enterTo="transform opacity-80"
                    leave="transition duration-250 ease-in"
                    leaveFrom="transform opacity-80"
                    leaveTo="transform opacity-0"
                    className="fixed w-screen h-screen left-0 top-0 bottom-0 hidden lg:block bg-neutral-900"
                    onClick={this.closeView}
                    onKeyDown={this.closeView}
                    role="none"
                />
            </Transition>
        )
    }
}
