import { HTMLAttributes, SVGProps } from 'react'

type ButtonProps = {
    label: string
    textColor?: string
    link?: string
    primary?: boolean
    secondary?: boolean
    colored?: boolean
    customColor?: string
    customImageStyle?: string
    image?: SVGProps<SVGSVGElement> | string
    imageOnRight?: boolean
    submitFunction?: () => void
}

const innerElement = (props: ButtonProps) => {
    return (
        <>
            {props.image !== undefined ? (
                props.imageOnRight ? null : typeof props.image === 'string' ? (
                    <img
                        className={props.customImageStyle + ' w-12 left-8'}
                        src={props.image}
                        alt="TODO: Make img emmet ADA-compliant (ie. add alt text)"
                    />
                ) : (
                    props.image
                )
            ) : null}
            <p className={'px-4 whitespace-nowrap lg:text-base'}>
                {props.link ? <a href={props.link}>{props.label}</a> : props.label}
            </p>
            {props.image !== undefined ? (
                props.imageOnRight ? (
                    typeof props.image === 'string' ? (
                        <img
                            className={props.customImageStyle + ' w-12 right-8'}
                            src={props.image}
                            alt="TODO: Make img emmet ADA-compliant (ie. add alt text)"
                        />
                    ) : (
                        props.image
                    )
                ) : null
            ) : null}
        </>
    )
}

export default function Button(props: ButtonProps & HTMLAttributes<HTMLDivElement>) {
    const color = props.primary
        ? 'bg-purple-500 hover:bg-violet-500 drop-shadow-lg transition-colors duration-200'
        : props.colored && !props.customColor
        ? 'bg-purple-500 hover:bg-purple-800 drop-shadow-md'
        : props.customColor
        ? props.customColor
        : 'bg-slate-300 hover:bg-slate-200 text-black drop-shadow-lg'

    const textColor = props.textColor !== undefined ? props.textColor : '' // If no color provided, let the page choose the color

    return (
        <button
            className={
                color +
                textColor +
                '  p-4 text-center font-medium rounded-3xl fit-content flex flex-row justify-center items-center min-h-fit h-12 ' +
                props.className
            }
        >
            {innerElement(props)}
        </button>
    )
}
