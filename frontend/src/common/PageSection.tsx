import { Component, HTMLAttributes } from 'react'

type PageSectionProps = {
    sectionName: string
    topSection?: boolean
    backgroundImage?: string
}
export default class PageSection extends Component<
    PageSectionProps & HTMLAttributes<HTMLDivElement>
> {
    render() {
        const background =
            'bg-gray-900 ' +
            (this.props.backgroundImage ? this.props.backgroundImage + ' bg-cover' : '') +
            (this.props.topSection ? 'bg-gradient-to-b from-gray-800 to-gray-900 ' : '')

        return (
            <div
                id={this.props.id?.toLowerCase()}
                className={
                    'flex min-h-screen h-fit w-full p-8 lg:p-24 xl:p-32 ' +
                    background +
                    (this.props.className ? this.props.className : '')
                }
                aria-label={`${this.props.sectionName} section`}
            >
                {this.props.children}
            </div>
        )
    }
}
