import utc from 'dayjs'

export const allowDocumentScroll = (canMove: boolean) => {
    if (canMove) document.body.classList.remove('overflow-hidden')
    else document.body.classList.add('overflow-hidden')
}

export const LabelColorMap: { [key: string]: string } = {
    web: 'bg-amber-100',
    game: 'bg-teal-100',
}

export const Footer = () => {
    return (
        <div className="h-fit bg-gray-900 w-full p-8 text-center">
            <p>michaelpmiddleton.com &copy; 2017 - {utc().year()}</p>
            <p>All Rights Reserved.</p>
        </div>
    )
}
