export const RequestHeader = {
    headers: {
        Authorization: `Bearer ${import.meta.env.VITE_API_TOKEN}`,
    },
};
