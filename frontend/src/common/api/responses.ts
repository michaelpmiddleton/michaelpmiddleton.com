// Interfaces for Strapi API responses:
export interface ResponseEntry<T> {
    id: number
    attributes: T
    locale?: string
}
export interface Response<T> {
    data: { data: ResponseEntry<T>[]; meta?: any } | ResponseEntry<T>
    status: number
}

// Strapi "Object" types
export type ImageResonse = {
    caption: string
    url: string
}
export type LabelResponse = {
    name: string
}

// Formats images in responses to match expected Image object for consumption
export const FormatImageResponse = (responseData: Response<ImageResonse>): ImageResonse => {
    let resp = responseData.data as ResponseEntry<ImageResonse>
    if (resp.attributes.url)
        return {
            caption: resp.attributes.caption,
            url: `${resp.attributes.url}`,
        }
    else
        return {
            caption: 'Unable to load image',
            url: 'https://via.placeholder.com/300.png',
        }
}

export const FormatLabelResponse = (responseData: Response<LabelResponse>): Array<string> => {
    if (Array.isArray(responseData.data)) {
        let cleanedLabels = Array<string>()
        responseData.data.map((labelEntry: ResponseEntry<LabelResponse>) => {
            cleanedLabels.push(labelEntry.attributes.name)
            return cleanedLabels
        })
        return cleanedLabels
    }
    return []
}
