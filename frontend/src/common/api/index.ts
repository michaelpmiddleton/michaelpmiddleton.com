export * from "./responses";
export * from "./requests";

const prefix = import.meta.env.PROD
    ? `${import.meta.env.VITE_API_BASE_URL}`
    : `${import.meta.env.VITE_API_LOCAL_BASE_URL}`;
export const api = `${prefix}/api`;
