import { InformationCircleIcon } from '@heroicons/react/24/solid'
import { ImageResonse } from './api/responses'
import { ProjectAttribute, ProjectType } from '.'
import { Transition } from '@headlessui/react'
import React from 'react'

export type Project = {
    id: number
    title: string
    description: string
    image?: ImageResonse | null
    locale: string
    labels?: Array<ProjectType>
    keywords?: Array<string>
    link?: string // FE element: Used for creating a "go to full portfolio button"
} & Partial<Record<ProjectAttribute, string>>

type ProjectTileProps = {
    skeleton?: boolean
    placeholder?: boolean
    project?: Project
    show?: boolean
    focusFunction?: () => void
}

export const SkeletonProjectTile = (
    <div className="w-full m-1 lg:m-0 h-48 lg:h-80 bg-slate-600 animate-pulse rounded-md" />
)

export function ProjectTile(props: ProjectTileProps) {
    return props.skeleton ? (
        SkeletonProjectTile
    ) : (
        <Transition
            show={props.show}
            enter="transition duration-150 ease-in-out"
            enterFrom="transform scale-0 opacity-0"
            enterTo="transform scale-100 opacity-100"
            leave="transition duration-100 ease-in"
            leaveFrom="transform scale-100 opacity-100"
            leaveTo="transform scale-0 opacity-0"
            className={
                'group cursor-pointer relative h-56 2xl:h-80 max-h-fit hover:shadow-md shadow-lg'
            }
        >
            <a
                href={props.project?.link ? props.project.link : ''}
                onClick={(event: React.MouseEvent) => {
                    if (!props.project?.link) event.preventDefault()
                    if (props.focusFunction && props.focusFunction !== undefined)
                        props.focusFunction()
                }}
            >
                {props.project?.image ? (
                    <img
                        src={props.project.image.url}
                        alt={props.project.image.caption}
                        className="rounded-3xl group-hover:brightness-0 w-full h-full group-hover:scale-105 transition-transform duration-150 ease-in-out"
                    />
                ) : (
                    <div className="w-full h-full bg-slate-600 group-hover:brightness-0 rounded-3xl group-hover:scale-105 scale-100 duration-150 ease-out brightness-100" />
                )}
                <p
                    className={
                        (!props.project?.image
                            ? 'opacity-100 ease-out '
                            : 'opacity-0 ease-in-out ') +
                        'block group-hover:opacity-100 group-hover:scale-110  absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 font-medium text-2xl transition-[opacity] transition-transform duration-150'
                    }
                >
                    {props.project?.title}
                </p>
                {props.placeholder ? null : (
                    <InformationCircleIcon className="hidden group-hover:block absolute bottom-4 right-4 w-8" />
                )}
            </a>
        </Transition>
    )
}
