import { Component } from 'react'
import { HomeIcon, Bars3Icon, XMarkIcon } from '@heroicons/react/24/solid'
import { GlobeAltIcon } from '@heroicons/react/24/outline'
import { allowDocumentScroll } from './Misc'

type Link = {
    name: string
    url: string
    icon?: any
}
interface DefaultNavbarState {
    drawerIsOpen: boolean
}
export default class Navbar extends Component<{}, DefaultNavbarState> {
    constructor(props: any) {
        super(props)
        this.state = { drawerIsOpen: false }
    }

    processLinks() {
        let listOfLinks = Array<Link>()

        if (window.location.pathname === '/') {
            listOfLinks.push({ name: 'Home', url: '/#home' })
            listOfLinks.push({ name: 'About', url: '/#about' })
            listOfLinks.push({ name: 'Portfolio', url: '/#recent-work' })
            listOfLinks.push({ name: 'Contact', url: '/#contact' })
        } else
            listOfLinks.push({
                name: 'Return to Homepage',
                url: '/',
                icon: (
                    <HomeIcon className="flex w-6 float float-left group-hover:text-violet-400" />
                ),
            })

        return listOfLinks.map((link) => (
            <a
                className="flex align-middle group justify-center"
                key={link.name}
                href={link.url}
                onClick={() => this.toggleNavbar(this.state.drawerIsOpen)}
            >
                {link.icon && link.icon}
                <p
                    className={
                        (link.icon ? 'pl-4 ' : '') + 'py-12 lg:py-8 group-hover:text-violet-400'
                    }
                >
                    {link.name}
                </p>
            </a>
        ))
    }

    toggleNavbar = (shouldFreeze: boolean) => {
        allowDocumentScroll(shouldFreeze)

        return this.setState((prevState) => ({
            ...prevState,
            drawerIsOpen: !prevState.drawerIsOpen,
        }))
    }

    render() {
        let links = this.processLinks()

        return (
            <>
                <Bars3Icon
                    className="fixed w-12 left-8 xl:left-16 top-8 xl:top-16 opacity-80 hover:opacity-100 z-40 cursor-pointer"
                    onClick={() => this.toggleNavbar(this.state.drawerIsOpen)}
                    onKeyDown={() => this.toggleNavbar(this.state.drawerIsOpen)}
                />
                <div
                    className={
                        'hidden lg:block opacity-70 fixed w-full h-full z-40 duration-100 ease-out ' +
                        (this.state.drawerIsOpen
                            ? 'bg-black'
                            : 'bg-transparent pointer-events-none')
                    }
                    onClick={() => this.toggleNavbar(this.state.drawerIsOpen)}
                    role="none"
                />
                {/* ^^^     Blurred Background preventing page clicks   ^^^ */}
                <div className="fixed top-0 w-full h-36 bg-gradient-to-b from-slate-900 lg:hidden z-30" />
                {/* ^^^     Background blur on mobile navbar icon  ^^^ */}
                {/* vvv     Actual navbar components     vvv */}
                <div
                    className={
                        'fixed flex flex-col lg:p-12 w-full lg:w-96 h-screen left-0 top-0 bottom-0 bg-gray-900 z-50 text-2xl text-gray-200 ease-in duration-100 justify-center ' +
                        (this.state.drawerIsOpen
                            ? 'translate-x-0'
                            : '-translate-x-full lg:-translate-x-96')
                    }
                >
                    <XMarkIcon
                        className="absolute top-8 right-8 w-12 lg:hidden"
                        onClick={() => this.toggleNavbar(this.state.drawerIsOpen)}
                    />
                    <div>{links}</div>
                    {/* TODO: Swap this with https://tailwindui.com/components/application-ui/forms/select-menus  */}
                    <GlobeAltIcon className="absolute left-6 bottom-4 w-16" />
                    <select
                        id="locale"
                        className="absolute w-full left-0 right-0 bottom-0 h-24 appearance-none bg-slate-800 bg-opacity-0 hover:bg-opacity-30 focus:bg-opacity-30 border-2 text-center text-white focus:outline-none border-none text-base"
                        disabled
                    >
                        <option value="en">English (US)</option>
                        <option value="fr">Français (France)</option>
                        <option value="de">Deutsch (Deutschland)</option>
                        <option value="ja">日本語</option>
                        <option value="ru">русский</option>
                    </select>
                </div>
            </>
        )
    }
}
