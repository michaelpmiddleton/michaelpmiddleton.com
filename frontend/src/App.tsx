import { Component } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Navbar from "./common/Navbar";
import { Homepage } from "./home";
import { Portfolio } from "./portfolio";
// import Homepage from 'components/Pages/Homepage';
// import Footer from 'components/Footer/Footer'

class App extends Component {
    constructor(props: any) {
        super(props);
        this.state = {
            canScroll: false,
        };
    }

    preventBodyScroll = (value: boolean) => {
        this.setState((prevState) => ({ ...prevState, canScroll: value }));
    };

    render() {
        return (
            <Router>
                <Navbar />
                <Routes>
                    <Route path="/">
                        <Route index element={<Homepage />} />
                        <Route path="portfolio" element={<Portfolio />} />
                    </Route>
                </Routes>
            </Router>
        );
    }
}

export default App;
