import { useEffect, useMemo, useRef, useState } from "react";
import { PageSection, Project, ProjectAttribute, ProjectTile, ProjectType } from "../common";
import { RequestHeader } from "../common/api/requests";
import { Response, FormatImageResponse, FormatLabelResponse } from "../common/api/responses";
import { ChevronUpDownIcon, XMarkIcon } from "@heroicons/react/24/solid";
import axios from "axios";
import ProjectDetailView from "../common/ProjectDetailView";
import { api } from "../common/api";
import { Listbox, Switch, Transition } from "@headlessui/react";

type ProjectAttributeSelectorProps = {
    text: string;
    isActive: boolean;
    onChangeFunction: () => void;
};
const ProjectAttributeSelector = (props: ProjectAttributeSelectorProps) => (
    <Switch.Group>
        <Switch
            checked={props.isActive}
            onChange={props.onChangeFunction}
            className={`${
                props.isActive
                    ? "bg-violet-500 hover:bg-violet-300"
                    : "bg-gray-200 hover:bg-gray-400"
            } relative inline-flex h-6 w-11 items-center rounded-full`}
        >
            <span
                className={`${
                    props.isActive ? "translate-x-6" : "translate-x-1"
                } inline-block h-4 w-4 transform rounded-full bg-white transition`}
            />
        </Switch>
        <Switch.Label className="inline-block text-lg px-2">{props.text}</Switch.Label>
    </Switch.Group>
);

const projectTypeMapper: Record<ProjectType, string> = {
    game: "Games",
    web: "Websites / Web Projects",
};

const ProjectTypeOption = (selector?: ProjectType) => (
    <Listbox.Option
        className="px-4 py-2 hover:bg-violet-200 first:rounded-t-2xl last:rounded-b-2xl"
        key={selector ? selector : "all"}
        value={selector ? selector : ""}
    >
        {selector ? projectTypeMapper[selector] : "All Projects"}
    </Listbox.Option>
);

export default function Portfolio() {
    const [search, setSearch] = useState("");
    const [focusedProject, setFocusedProject] = useState<Project>();
    const [projects, setProjects] = useState<Array<Project>>([]);
    const [filters, setFilters] = useState<Array<ProjectAttribute>>([]);
    const [projectType, setProjectType] = useState<ProjectType>();

    const projectsRef = useRef<HTMLDivElement>(null);
    const searchRef = useRef<HTMLInputElement>(null);

    const toggleFilter = (toToggle: ProjectAttribute) => {
        if (filters.includes(toToggle)) setFilters(filters.filter((value) => value !== toToggle));
        else setFilters([...filters, toToggle]);
    };

    const clearSearch = () => {
        if (searchRef.current) {
            searchRef.current.value = "";
        }
    };

    const relevantProjects = useMemo(() => {
        let filteredProjects: any[];
        if (projects.length > 0) {
            // Filtering -- Labels
            filteredProjects = projects;

            // Filtering -- Checkboxes
            filters.map((filter: ProjectAttribute) => {
                filteredProjects = (filteredProjects as Array<Project>).filter(
                    (project: Project) => {
                        return project[filter] && project[filter] !== null;
                    }
                );
            });

            // Filtering -- Project types
            if (projectType) {
                filteredProjects = filteredProjects.filter((project: Project) => {
                    return project.labels?.includes(projectType);
                });
            }

            // Filtering -- Search
            if (search !== "")
                filteredProjects = filteredProjects.filter((project: Project) =>
                    project.title.toLowerCase().includes(search)
                );
        } else filteredProjects = [...new Array(6)];

        console.log(filteredProjects);

        return projects.length === 0
            ? filteredProjects.map((_, index) => <ProjectTile key={`skeleton${index}`} skeleton />)
            : projects.map((proj: Project) => (
                  <ProjectTile
                      key={proj.title}
                      project={proj}
                      show={filteredProjects.includes(proj)}
                      focusFunction={() => setFocusedProject(proj)}
                  />
              ));
    }, [projects, search, projectType, filters]);

    // Set the projects to API response once we have data...
    useEffect(() => {
        // If projects has been set, this function doesn't need to fire again -- return
        if (projects.length > 0) return;

        const getApiData = async () => {
            const data = await axios.get<{
                data: Response<Project>;
            }>(`${api}/projects?locale=en&populate=*`, RequestHeader);
            return data;
        };

        getApiData().then(
            (response) => {
                const projectsCleaned: Array<Project> = [];

                if (Array.isArray(response.data.data)) {
                    response.data.data.map((rawProj) => {
                        let cleaned = rawProj.attributes;

                        // Clean image
                        cleaned.image = cleaned.image.data
                            ? FormatImageResponse(cleaned.image)
                            : null;

                        // Clean labels
                        cleaned.labels = cleaned.labels
                            ? FormatLabelResponse(cleaned.labels)
                            : null;

                        // Set project ID
                        cleaned.id = rawProj.id;

                        // Remove extra Strapi field
                        delete cleaned["localizations"];

                        projectsCleaned.push(cleaned);
                        return projectsCleaned;
                    });
                }

                setProjects(projectsCleaned);
            },
            (error) => {
                console.log(error);
                // ClientErrorToast(
                //     'An error occurred fetching projects. Please refresh or try again later.'
                // )
            }
        );
    }, [projects]);

    return (
        <PageSection sectionName="Portfolio" className="flex-col">
            <ProjectDetailView
                isShowing={focusedProject !== undefined}
                projectData={focusedProject && focusedProject}
                unfocus={() => setFocusedProject(undefined)}
            />
            <h2 className="text-5xl text-center lg:text-left pl-4">My Portfolio</h2>
            <div
                className={`flex flex-col bg-gray-900 sticky top-0 lg:flex-row gap-4 py-8 px-4 place-items-end lg:place-items-center z-10 ${
                    focusedProject === undefined ? "" : " pointer-events-none"
                }`}
            >
                <div className="relative w-5/6 lg:inline-block lg:w-1/4 lg:grow">
                    <div className="absolute inset-y-0 right-0 flex items-center pr-4">
                        {<XMarkIcon className="w-4 lg:hidden" onClick={() => clearSearch()} />}
                    </div>

                    <input
                        ref={searchRef}
                        className="block appearance-none bg-slate-800 bg-opacity-0 focus:bg-opacity-40  border-2 text-white placeholder-white focus:outline-none focus:border-purple-400 border-slate-400 rounded-3xl lg:rounded-xl p-2 px-4 w-full"
                        type="text"
                        placeholder="Type to search"
                        onChange={(element) => setSearch(element.target.value)}
                    />
                </div>
                <div className="relative group appearance-none  w-5/6 lg:w-1/4 z-10">
                    <Listbox value={projectType} onChange={setProjectType}>
                        <Listbox.Button className="relative w-full items-center  bg-opacity-0 focus:bg-opacity-40 border-2 text-white placeholder-white focus:outline-none focus:border-purple-400 border-slate-400 rounded-3xl lg:rounded-xl p-2">
                            <p className="clear-none">
                                {projectType ? projectTypeMapper[projectType] : "All Projects"}
                            </p>
                            <ChevronUpDownIcon className="group-hover:fill-violet-300 h-6 top-1/2 -translate-y-1/2 absolute right-2" />
                        </Listbox.Button>
                        <Transition
                            enterFrom="transform scale-95 opacity-0"
                            enterTo="transform scale-100 opacity-100"
                            leave="transition duration-150 ease-out"
                            leaveFrom="transform scale-100 opacity-100"
                            leaveTo="transform scale-95 opacity-0"
                        >
                            <Listbox.Options className="absolute w-full -bottom-2 translate-y-full bg-gray-300 text-black rounded-2xl">
                                {ProjectTypeOption()}
                                {ProjectTypeOption("game")}
                                {ProjectTypeOption("web")}
                            </Listbox.Options>
                        </Transition>
                    </Listbox>
                </div>
                <ProjectAttributeSelector
                    text="Source Code Available"
                    isActive={filters.includes("sourceCode")}
                    onChangeFunction={() => toggleFilter("sourceCode")}
                />
                <ProjectAttributeSelector
                    text="Download Available"
                    isActive={filters.includes("download")}
                    onChangeFunction={() => toggleFilter("download")}
                />
            </div>

            <div
                className={`py-4 lg:p-4 grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3  gap-6 h-4/5 overflow-y-auto auto-rows-min  ${
                    focusedProject === undefined ? "" : " pointer-events-none"
                }`}
                ref={projectsRef}
            >
                {relevantProjects}
            </div>
        </PageSection>
    );
}
