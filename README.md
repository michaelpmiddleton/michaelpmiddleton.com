# michaelpmiddleton.com

[![Netlify Status](https://api.netlify.com/api/v1/badges/ee48e33d-a9dd-4a53-b874-55b9335763c2/deploy-status)](https://app.netlify.com/sites/michaelpmiddleton/deploys) ![Heroku](https://pyheroku-badge.herokuapp.com/?app=michaelpmiddleton)  
This repository comprises the code that makes up my portfolio. I make some changes here and there, but it's mainly a CMS-based site so the only changes done here are service improvements/additions or complete structural schenanigans. (Don't go back to v3 unless you want to learn why I'm _not_ a PHP developer.)

## What's next for the site?

Wow, ok, I didn't think anyone actually would be interested in that question. You can check [here](https://gitlab.com/michaelpmiddleton/michaelpmiddleton.com/-/issues) to see a list of features I'm planning to implement/bugs I know about.

## What did I learn/use to learn for this site?

- Usman Khalil's "[Enforce Husky Pre-Commit With ESLint & Prettier In Monorepo](https://dev.to/monfernape/enforce-husky-pre-commit-with-eslint-prettier-in-monorepo-55jc)" dev.to article for linting and formatting

## Notice a bug?

Cool! Thanks so much for letting me know [here](www.michaelpmiddleton.com/#contact) or, if that fails, send me an [email](mailto:contact@michaelpmiddleton.com). You're truly a homie!

## Buy me a coffee?

Fam -- this is my personal website. If you're really willing to give me money, consider donating to my charity of choice instead: [Lions Internaional Foundation](https://www.lionsclubs.org/en/donate). You are a gentleperson and a scholar.
